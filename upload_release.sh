#!/bin/bash

RELEASE_NAME="$1"
TAG_NAME="$2"
PROJECT_ID="$3"
DESCRIPTION="$4"
PRIVATE_TOKEN="$5"


if [ "$5" == "" ]; then
    echo "Missing parameter! Parameters are RELEASE_NAME, TAG_NAME, PROJECT_ID, DESCRIPTION, FILEPATH and PRIVATE_TOKEN.";
    exit 1;
fi

curl --request POST\
     --header 'Content-Type: application/json'\
     --header "Private-Token: ${PRIVATE_TOKEN}"\
     --data-binary "{\"name\": \"${RELEASE_NAME}\", \"tag_name\": \"${TAG_NAME}\", \"description\": \"${DESCRIPTION}\", \"assets\": { \"links\": [{ \"name\": \"linux-${TAG_NAME}-setup.AppImage\", \"url\": \"https://storage.cloud.google.com/release-mspr-evol/release-${TAG_NAME}/linux_setup.AppImage\" }, { \"name\": \"windows-${TAG_NAME}-setup.exe\", \"url\": \"https://storage.cloud.google.com/release-mspr-evol/release-${TAG_NAME}/windows_setup.exe\" }] }}"\
     "https://gitlab.com/api/v4/projects/${PROJECT_ID}/releases" 

echo