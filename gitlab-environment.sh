#!/bin/bash

ENVIRONMENT_TYPE="$1"
TAG_VERSION="$2"
EXTERNAL_IP="$3"
EXTERNAL_PORT="$4"
PROJECT_ID="$5"
GITLAB_TOKEN="$6"


if [ "$6" == "" ]; then
    echo "Missing parameter! Parameters are ENVIRONMENT_TYPE, TAG_VERSION, EXTERNAL_IP, EXTERNAL_PORT, PROJECT_ID and GITLAB_TOKEN.";
    exit 1;
fi

curl --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" --data "name=${ENVIRONMENT_TYPE}-${TAG_VERSION}&external_url=http://${EXTERNAL_IP}:${EXTERNAL_PORT}" "https://gitlab.com/api/v4/projects/${PROJECT_ID}/environments"

echo