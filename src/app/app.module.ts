import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { MatToolbarModule, MatButtonModule, MatProgressSpinnerModule, MatSnackBarModule } from '@angular/material';

import { ElectronService } from './electron/electron.service';
import { AppRoutingModule } from './app-routing.module';

import { HttpClientModule } from '@angular/common/http';

import { NgxDropzoneModule } from 'ngx-dropzone';

/* COMPONENTS */
import { HomeComponent } from './components/home/home.component';
import { ImportComponent } from './components/import/import.component';
import { ImportService } from './services/import.service';
import { MaintenanceComponent } from './components/maintenance/maintenance.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    ImportComponent,
    MaintenanceComponent
  ],
  imports: [
    AppRoutingModule,
    BrowserModule,
    BrowserAnimationsModule,
    MatToolbarModule,
    MatButtonModule,
    MatProgressSpinnerModule,
    MatSnackBarModule,
    HttpClientModule,
    NgxDropzoneModule
  ],
  providers: [
    ElectronService,
    ImportService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
