import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './components/home/home.component';
import { MaintenanceComponent } from './components/maintenance/maintenance.component';
import { MaintenanceService, MaintenanceServiceNegate } from './services/maintenance.service';

const routes: Routes = [
  {
    path: '',
    canActivate: [MaintenanceServiceNegate],
    component: HomeComponent
  },
  {
    path: 'maintenance',
    canActivate: [MaintenanceService],
    component: MaintenanceComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true })],
  exports: [RouterModule]
})

export class AppRoutingModule { }
