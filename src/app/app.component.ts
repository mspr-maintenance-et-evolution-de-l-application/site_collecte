import { Component, OnInit } from '@angular/core';
import { ElectronService } from 'src/app/electron/electron.service';
import { MaintenanceService } from './services/maintenance.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

  /**
  * Appellation de la page
  */
  title: string = 'sitecollecte';

  constructor(public electronService: ElectronService, private maintenanceService: MaintenanceService, private router: Router) {

    this.maintenanceService.connect();

    if (electronService.isElectron()) {
      console.log('Mode electron');
      // Check if electron is correctly injected (see externals in webpack.config.js)
      console.log('c', electronService.ipcRenderer);
      // Check if nodeJs childProcess is correctly injected (see externals in webpack.config.js)
      console.log('c', electronService.childProcess);
    } else {
      console.log('Mode web');
    }
  }

  /**
    * Méthode d'initialisation
  */
  ngOnInit() {

    this.maintenanceService.getMaintenance().subscribe((maintenanceState: boolean) => {
      if(maintenanceState) {
        this.router.navigate(['maintenance']);
      } else {
        this.router.navigate(['']);
      }
    })

  }

}
