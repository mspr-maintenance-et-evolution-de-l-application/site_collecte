import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ImportService {

  endpointURL: string = 'http://35.232.207.23:8080/dataset/uploads';

  /**
   * Liste des fichiers à upload et déposez dans la zone de dépôt
  */
  files: File[] = [];

  /**
   * Gestion de l'abonnement pour la liste des fichiers
  */
  FileSubject: Subject<File[]> = new Subject<File[]>();

  constructor(private http: HttpClient) { }

  /**
   * Méthode d'ajout après sélection d'un/des fichiers à la liste des fichiers
  */
  emitFileSubject(): void {
    this.FileSubject.next(this.files);
  }

  /**
   * Méthode d'ajout après sélection d'un/des fichiers à la liste des fichiers
  */
  onSelect(event: any): void {
    this.files.push(...event.addedFiles);
    this.emitFileSubject();
  }

  /**
   * Méthode de suppression d'un fichier après un ajout précédent à la liste des fichiers
  */
  onRemove(event: any): void {
    this.files.splice(this.files.indexOf(event), 1);
    this.emitFileSubject();
  }

  /**
   * Méthode pour réinitialiser la liste des fichiers
  */
  onRemoveAll(): void {
    this.files = [];
    this.emitFileSubject();
  }

  /**
   * Méthode pour envoyer tous les fichiers uploaded à l'API
  */
  onUploadAll(): void {

    var re = /(?:\.([^.]+))?$/;
    var ext = re.exec(this.files[0].name)[1];

    let formData: FormData = new FormData();
    formData.append('format', ext);

    this.files.forEach(file => {
      formData.append('files', file);
    });

    this.http.post(this.endpointURL, formData).subscribe((res) => {
      console.log(res);
    });

    // Nous effaçons le tableau une fois l'opération terminée !
    this.onRemoveAll();
  }

}
