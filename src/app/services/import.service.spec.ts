import { TestBed } from '@angular/core/testing';

import { ImportService } from './import.service';
import { HttpClientModule } from '@angular/common/http';

export class EventInterface {
  addedFiles: File[];
}

describe('ImportService', () => {

  let service: ImportService;

  let addedFiles: File[] = [];

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientModule
      ]
    });
    service = TestBed.get(ImportService);
  });

  it('should create', () => {
    expect(service).toBeTruthy();
  });

  it('onSelectTest(event: any)', () => {
    let event: EventInterface = new EventInterface();
    let fileToAdd = new File(["test1;nom_de__data;data"], "test.csv");
    addedFiles.push(fileToAdd);
    event.addedFiles = addedFiles;
    
    service.onSelect(event);
    expect(service.files.length).toEqual(1);
  });

  it('onRemoveTest(event: any)', () => {
    let fileToDelete = new File(["test1;nom_de__data;data"], "test.csv");
    service.files = [ fileToDelete ];
    service.onRemove(fileToDelete);
    expect(service.files.length).toEqual(0);
  });

  it('onRemoveAllTest(event:any)', () => {
    let fileToDelete1 = new File(["test1;nom_de__data;data"], "test.csv");
    let fileToDelete2 = new File(["test1;nom_de__data;data"], "test.csv");
    service.files = [ fileToDelete1, fileToDelete2 ];
    service.onRemoveAll();
    expect(service.files.length).toEqual(0);
  });
});
