import { Injectable, OnInit } from '@angular/core';
import { CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree } from '@angular/router';
import * as io from 'socket.io-client';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class MaintenanceService implements CanActivate {

  /**
   * Socket de connexion pour la mise en maintenance
  */
  private socket;

  /**
   * Boolean de mise en maintenance du site ! 
  */
  maintenanceBool: boolean = false;

  constructor() { }

  /**
   * Méthode de connexion au serveur de maintenance
  */
  connect(): void {

    if (this.socket == null) {
      this.socket = io("http://34.74.151.135:8080");
    }

    this.socket.on('maintenance_res', function (maintenanceBool) {
      this.maintenanceBool = maintenanceBool;
    });

    return this.socket;
  }

  /**
    * Méthode d'abonnement au statut de maintenance (enable or not)
  */
  public getMaintenance(): Observable<boolean> {
    let observable = new Observable<boolean>(observer => {
      this.socket.on('maintenance_res', function (maintenanceBool) {
        this.maintenanceBool = maintenanceBool;
        observer.next(maintenanceBool);
      });
      return () => {
        this.socket.disconnect();
      };
    })
    return observable;
  }

  /**
    * Méthode d'authorisation du routing sur la page de Maintenance'
  */
  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean | UrlTree | Observable<boolean | UrlTree> | Promise<boolean | UrlTree> {

    if (this.maintenanceBool) {
      return false;
    } else {
      return true;
    }

  }

}

@Injectable({
  providedIn: 'root'
})
export class MaintenanceServiceNegate implements CanActivate {

  /**
   * Socket de connexion pour la mise en maintenance
  */
  private socket;

  /**
   * Boolean de mise en maintenance du site ! 
  */
  maintenanceBool: boolean = false;

  constructor(private maintenanceService: MaintenanceService, private router: Router) {

    this.socket = this.maintenanceService.connect();

    this.socket.on('maintenance_res', function (maintenanceBool) {
      this.maintenanceBool = maintenanceBool;
    });

  }

  /**
    * Méthode d'authorisation du routing sur la page de Maintenance'
  */
  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean | UrlTree | Observable<boolean | UrlTree> | Promise<boolean | UrlTree> {

    if (!this.maintenanceBool) {
      return true;
    } else {
      return false;
    }

  }

}
