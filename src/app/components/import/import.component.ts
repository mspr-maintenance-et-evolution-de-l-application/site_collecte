import { Component, OnInit } from '@angular/core';
import { ImportService } from 'src/app/services/import.service';

@Component({
  selector: 'app-import',
  templateUrl: './import.component.html',
  styleUrls: ['./import.component.scss']
})
export class ImportComponent implements OnInit {

  /**
   * Liste des fichiers à upload et déposez dans la zone de dépôt
  */
  files: File[] = [];

  constructor(private importService: ImportService) { }

  /**
   * Méthode d'initialisation 
  */
  ngOnInit() {

    this.importService.FileSubject.subscribe( (files: File[]) => {
      this.files = files;
    });

  }

  /**
   * Méthode d'ajout d'un fichier à la liste des fichiers via le service "ImportService"
  */
  onSelect(event: any): void {
    console.log(event);
    this.importService.onSelect(event);
  }

  /**
   * Méthode de suppression d'un fichier à la liste des fichiers via le service "ImportService"
  */
  onRemove(event: any): void {
    console.log(event);
    this.importService.onRemove(event);
  }

}
