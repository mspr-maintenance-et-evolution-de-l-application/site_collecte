import { Component, OnInit } from '@angular/core';
import { ImportService } from 'src/app/services/import.service';
import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  constructor(private importService: ImportService, private snackBar: MatSnackBar) { }

  /**
     * Méthode d'initialisation
  */
  ngOnInit() {
  }

  /**
   * Méthode pour réinitialiser la liste des fichiers via le service "ImportService"
  */
  onRemoveAll(): void {
    this.importService.onRemoveAll();
    this.openSnackBar("Les fichiers ont bien été supprimés !");
  }

  /**
   * Méthode pour uploader tous les fichiers sur l'API
  */
  onUploadAll(): void {
    this.importService.onUploadAll();
    this.openSnackBar("Les fichiers ont bien été envoyés !");
  }

  /**
   * Méthode pour ouvrir un popup avec un message spécifique
  */
  openSnackBar(message: string) {
    this.snackBar.open(message, null, {
      duration: 2000,
    });
  }

}
