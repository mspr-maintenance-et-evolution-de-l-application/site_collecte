FROM node:12.12.0
EXPOSE 4200
WORKDIR /etc
COPY . .
CMD npm install && npm run ng:serve